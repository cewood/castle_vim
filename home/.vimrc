" Inspiration taken from the following sources...
"
" https://raw.github.com/terryma/dotfiles/master/.vimrc



" Disable vi-compatibility
set nocompatible

"===============================================================================
" NeoBundle
"===============================================================================

if has ('vim_starting')
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

call neobundle#rc(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'Shougo/vimproc', { 'build': {
      \   'windows': 'make -f make_mingw32.mak',
      \   'cygwin': 'make -f make_cygwin.mak',
      \   'mac': 'make -f make_mac.mak',
      \   'unix': 'make -f make_unix.mak',
      \ } }


" Fuzzy search
NeoBundle 'Shougo/unite.vim'
"NeoBundle 'Shougo/unite-outline'
NeoBundle 'Shougo/unite-help'
NeoBundle 'Shougo/unite-session'
NeoBundle 'Shougo/neomru.vim'
NeoBundle 'thinca/vim-unite-history'
"NeoBundle 'kien/ctrlp.vim.git'
NeoBundle 'm2mdas/unite-file-vcs'


" Buffers
NeoBundle 'szw/vim-ctrlspace'
NeoBundle 'moll/vim-bbye'


" Code completion
NeoBundle "Shougo/neocomplete.vim"
"NeoBundle'Shougo/neocomplcache'
NeoBundle "klen/python-mode"


" Marks
NeoBundle 'kshenoy/vim-signature'


" Comments
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'tpope/vim-commentary.git'


" File browsing
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'Shougo/vimfiler'


" Syntax checker
NeoBundle 'scrooloose/syntastic'


" Shell
NeoBundle 'Shougo/vimshell'
NeoBundle 'thinca/vim-quickrun'
NeoBundle 'tpope/vim-dispatch'


" File types
NeoBundle 'rodjek/vim-puppet'
"NeoBundle 'sheerun/vim-polyglot'


" Git
NeoBundle 'tpope/vim-fugitive'


" Motions
NeoBundle 'Lokaltog/vim-easymotion'


" Text Objects
NeoBundle 'tpope/vim-surround'


" Indentation & Tabs
NeoBundle 'tpope/vim-sensible.git' " Needs to precede vim-sleuth
NeoBundle 'tpope/vim-sleuth.git'
NeoBundle 'godlygeek/tabular.git'


" Tags
"NeoBundle 'xolox/vim-easytags'
NeoBundle 'majutsushi/tagbar'


" Status line
NeoBundle 'bling/vim-airline' " 100x faster than Powerline! :)


" Misc
NeoBundle 'sjl/gundo.vim'
NeoBundle 'mhinz/vim-signify'
NeoBundle 'terryma/vim-multiple-cursors'
NeoBundle 'tpope/vim-repeat.git'
NeoBundle 't9md/vim-quickhl'
NeoBundle 'tpope/vim-scriptease.git'



" Color themes
"NeoBundle 'altercation/vim-colors-solarized'
"NeoBundle 'tomasr/molokai'
"NeoBundle 'Lokaltog/vim-distinguished'
NeoBundle 'chriskempson/base16-vim'
"NeoBundle 'tpope/vim-vividchalk'
"NeoBundle 'chriskempson/tomorrow-theme', {'rtp': 'vim'}
"NeoBundle 'rainux/vim-desert-warm-256'
"NeoBundle 'nanotech/jellybeans.vim'
"NeoBundle 'vim-scripts/wombat256.vim'
"NeoBundle 'flazz/vim-colorschemes'
"NeoBundle 'altercation/vim-colors-solarized.git'
"NeoBundle 'sickill/vim-monokai'
"NeoBundle 'tomasr/molokai'
"NeoBundle 'larssmit/vim-getafe'
"NeoBundle 'junegunn/seoul256.vim'
"NeoBundle 'nelstrom/vim-blackboard.git'
"NeoBundle 'zeis/vim-kolor'
"NeoBundle 'morhetz/gruvbox'
"NeoBundle 'noahfrederick/Hemisu'
"NeoBundle 'jtai/vim-womprat'
"NeoBundle 'tristen/superman'
"NeoBundle 'sickill/vim-sunburst'
"NeoBundle 'trapd00r/neverland-vim-theme'
"NeoBundle 'chriskempson/vim-tomorrow-theme'
"NeoBundle 'baskerville/bubblegum'
"NeoBundle 'Pychimp/vim-luna'

filetype plugin indent on
syntax enable

NeoBundleCheck


"===============================================================================
" Local Settings
"===============================================================================

try
  source ~/.vimrc.local
catch
endtry


"===============================================================================
" General Settings
"===============================================================================

"syntax on
"syntax enable " Don't override our settings, as opposed to using `syntax on`

" Always splits to the right and below
set splitright
set splitbelow

" 256bit terminal
set t_Co=256

" Colorscheme
silent! colorscheme base16-default

" Tell Vim to use dark background
set background=dark

" Sets how many lines of history vim has to remember
set history=10000

" Open all folds initially
set foldmethod=indent
set foldlevelstart=99

set wildmode=list:longest,full
set wildmenu "turn on wild menu

" Allow changing buffer without saving it first
set hidden

" Set backspace config
set backspace=eol,start,indent

" Case insensitive search
set ignorecase
set smartcase

" Make search act like search in modern browsers
set incsearch

" Don't show matching brackets
"set noshowmatch

" Show incomplete commands
set showcmd

" Turn off sound
set vb
set t_vb=

" Always show the statusline
set laststatus=2

" Explicitly set encoding to utf-8
set encoding=utf-8

" Column width indicator
set colorcolumn=+1

"" Reload vimrc when edited
"autocmd MyAutoCmd BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc
"      \ so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif

try
  lang en_us
catch
endtry

" Turn backup off
set nobackup
set nowritebackup
set noswapfile

" Writes to the unnamed register also writes to the * and + registers. This
" makes it easy to interact with the system clipboard
if has ('unnamedplus')
  set clipboard=unnamedplus
else
  set clipboard=unnamed
endif


"===============================================================================
" Function Key Mappings
"===============================================================================

" <F1>: Help
nmap <F1> [unite]h

" <F2>: Open Vimfiler

" <F3>: Gundo
nnoremap <F3> :<C-u>GundoToggle<CR>

" <F4>: Save session
nnoremap <F4> :<C-u>UniteSessionSave


"===============================================================================
" Syntastic
"===============================================================================

" Enable Syntastic
"  Originally for working with Puppet files, but can
"  obviously be used for other files with syntax support
let g:syntastic_enable_signs = 1



"===============================================================================
" Ctrl-P config
"===============================================================================

" Ctlr-P {{{2
let g:ctrlp_jump_to_buffer = 0
let g:ctrlp_working_path_mode = 0
"let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = 'find %s -type f'
let g:ctrlp_map = '<Leader>pp'  " Is this needed?
let g:ctrlp_cmd = 'CtrlPMRU'
let g:ctrlp_extensions = ['Z', 'F']



"===============================================================================
" Unite
"===============================================================================

" Use the fuzzy matcher for everything
" call unite#filters#matcher_default#use(['matcher_fuzzy'])
" Use the rank sorter for everything
" call unite#filters#sorter_default#use(['sorter_rank'])

" Set up some custom ignores
call unite#custom_source('file_rec,file_rec/async,file_mru,file,buffer,grep',
      \ 'ignore_pattern', join([
      \ '\.git/',
      \ 'git5/.*/review/',
      \ 'google/obj/',
      \ 'tmp/',
      \ '.sass-cache',
      \ 'node_modules/',
      \ 'bower_components/',
      \ 'dist/',
      \ ], '\|'))

" Map space to the prefix for Unite
nnoremap [unite] <Nop>
nmap <space> [unite]


" General fuzzy search
nnoremap <silent> [unite]<space> :<C-u>Unite
      \ -buffer-name=files buffer file_mru bookmark file_rec/async<CR>

" Quick registers
nnoremap <silent> [unite]r :<C-u>Unite -buffer-name=register register<CR>

nnoremap <silent> [unite]u :<C-u>Unite -buffer-name=buffers file_mru buffer<CR>
" Quick buffer and mru

" Quick yank history
nnoremap <silent> [unite]y :<C-u>Unite -buffer-name=yanks history/yank<CR>

" Quick outline
nnoremap <silent> [unite]o :<C-u>Unite -buffer-name=outline -vertical outline<CR>

" Quick sessions (projects)
"nnoremap <silent> [unite]p :<C-u>Unite -buffer-name=sessions session<CR>

" Quick sources
nnoremap <silent> [unite]a :<C-u>Unite -buffer-name=sources source<CR>

" Quick snippet
nnoremap <silent> [unite]s :<C-u>Unite -buffer-name=snippets snippet<CR>

" Quickly switch lcd
nnoremap <silent> [unite]d
      \ :<C-u>Unite -buffer-name=change-cwd -default-action=lcd directory_mru<CR>

" Quick file search
nnoremap <silent> [unite]f :<C-u>Unite -buffer-name=files file_rec/async file/new<CR>

" Quick grep from cwd
nnoremap <silent> [unite]g :<C-u>Unite -buffer-name=grep grep:.<CR>

" Quick help
nnoremap <silent> [unite]h :<C-u>Unite -buffer-name=help help<CR>

" Quick line using the word under cursor
nnoremap <silent> [unite]l :<C-u>UniteWithCursorWord -buffer-name=search_file line<CR>

" Quick MRU search
nnoremap <silent> [unite]m :<C-u>Unite -buffer-name=mru file_mru<CR>

" Quick find
nnoremap <silent> [unite]n :<C-u>Unite -buffer-name=find find:.<CR>

" Quick commands
nnoremap <silent> [unite]c :<C-u>Unite -buffer-name=commands command<CR>

" Quick bookmarks
nnoremap <silent> [unite]b :<C-u>Unite -buffer-name=bookmarks bookmark<CR>

" Fuzzy search from current buffer
" nnoremap <silent> [unite]b :<C-u>UniteWithBufferDir
      " \ -buffer-name=files -prompt=%\  buffer file_mru bookmark file<CR>

" Quick commands
nnoremap <silent> [unite]; :<C-u>Unite -buffer-name=history history/command command<CR>

" Custom Unite settings
"autocmd MyAutoCmd FileType unite call s:unite_settings()
function! s:unite_settings()

  nmap <buffer> <ESC> <Plug>(unite_exit)
  imap <buffer> <ESC> <Plug>(unite_exit)
  " imap <buffer> <c-j> <Plug>(unite_select_next_line)
  imap <buffer> <c-j> <Plug>(unite_insert_leave)
  nmap <buffer> <c-j> <Plug>(unite_loop_cursor_down)
  nmap <buffer> <c-k> <Plug>(unite_loop_cursor_up)
  imap <buffer> <c-a> <Plug>(unite_choose_action)
  imap <buffer> <Tab> <Plug>(unite_exit_insert)
  imap <buffer> jj <Plug>(unite_insert_leave)
  imap <buffer> <C-w> <Plug>(unite_delete_backward_word)
  imap <buffer> <C-u> <Plug>(unite_delete_backward_path)
  imap <buffer> '     <Plug>(unite_quick_match_default_action)
  nmap <buffer> '     <Plug>(unite_quick_match_default_action)
  nmap <buffer> <C-r> <Plug>(unite_redraw)
  imap <buffer> <C-r> <Plug>(unite_redraw)
  inoremap <silent><buffer><expr> <C-s> unite#do_action('split')
  nnoremap <silent><buffer><expr> <C-s> unite#do_action('split')
  inoremap <silent><buffer><expr> <C-v> unite#do_action('vsplit')
  nnoremap <silent><buffer><expr> <C-v> unite#do_action('vsplit')

  " let unite = unite#get_current_unite()
  if unite.buffer_name =~# '^search'
    nnoremap <silent><buffer><expr> r     unite#do_action('replace')
  else
    nnoremap <silent><buffer><expr> r     unite#do_action('rename')
  endif

  nnoremap <silent><buffer><expr> cd     unite#do_action('lcd')

  " Using Ctrl-\ to trigger outline, so close it using the same keystroke
  if unite.buffer_name =~# '^outline'
    imap <buffer> <C-\> <Plug>(unite_exit)
  endif

  " Using Ctrl-/ to trigger line, close it using same keystroke
  if unite.buffer_name =~# '^search_file'
    imap <buffer> <C-_> <Plug>(unite_exit)
  endif
endfunction

" Start in insert mode
let g:unite_enable_start_insert = 1

let g:unite_data_directory = "~/.unite"

" Enable short source name in window
" let g:unite_enable_short_source_names = 1

" Enable history yank source
let g:unite_source_history_yank_enable = 1

" Open in bottom right
let g:unite_split_rule = "botright"

" Shorten the default update date of 500ms
let g:unite_update_time = 200

let g:unite_source_file_mru_limit = 1000
let g:unite_cursor_line_highlight = 'TabLineSel'
" let g:unite_abbr_highlight = 'TabLine'

let g:unite_source_file_mru_filename_format = ':~:.'
let g:unite_source_file_mru_time_format = ''



"===============================================================================
" Legacy Config
"===============================================================================

set nojoinspaces         " What am I using this for?
set autochdir            " Also what's this for?
set directory=~/.vimtmp/ " Do I need this?
set guioptions-=T        " remove toolbar
set title                " Set the terminal title
set visualbell           " Visual bell, duh!
setlocal spell
filetype off             " required! by Vundle originaly, iirc?

" Git commit cursor positioning
autocmd FileType gitcommit call setpos('.', [0, 1, 1, 0])

" Fix for not detecting puppet filetype properly :s
au! BufRead,BufNewFile *.pp setfiletype puppet

" Tagbar/ctags support for Puppet
let g:tagbar_type_puppet = {
    \ 'ctagstype': 'puppet',
    \ 'kinds': [
        \'c:class',
        \'s:site',
        \'n:node',
        \'d:definition'
      \]
    \}


" Alias to make Bdelete easier
command! -bang -complete=buffer -nargs=? Bc Bdelete<bang> <args>

" SuperRetab command, for easier/smart tab conversion
command! -nargs=1 -range SuperRetab <line1>,<line2>s/\v%(^ *)@<= {<args>}/\t/g



"===============================================================================
" Normal Mode Ctrl Key Mappings
"===============================================================================

" Ctrl-e: Find (e)verything
nmap <c-e> [unite]f

" Ctrl-r: Command history using Unite, this matches my muscle memory in zsh
"silent! nunmap <c-r>
"nmap <c-r> [unite];

" Ctrl-y: Yanks
nmap <c-y> [unite]y

" Ctrl-ss: (S)earch word under cur(s)or in current directory
nnoremap <c-s><c-s> :Unite grep:.::<C-r><C-w><CR>
" Ctrl-sd: (S)earch word in current (d)irectory (prompt for word)
nnoremap <c-s><c-d> :Unite grep:.<CR>
" Ctrl-sf: Quickly (s)earch in (f)ile
nmap <c-s><c-f> [unite]l

" Ctrl-v: Paste (works with system clipboard due to clipboard setting earlier)
" nnoremap <c-v> p



"===============================================================================
" Visual Mode Ctrl Key Mappings
"===============================================================================

" Ctrl-c: Copy (works with system clipboard due to clipboard setting)
vnoremap <c-c> y`]


" Puppet ctags support
set iskeyword=-,:,@,48-57,_,192-255

" Puppet tag navigating fix
au FileType puppet setlocal isk+=:


